import { ValidatorDefinitionsLoader } from './definitions-loader.service';
import { DoubleAgentValidator } from './validator.service';
// import * as fs from 'fs';
import * as jsdomNS from 'jsdom';


// import { expect } from 'chai';
import { InTestRawLoader } from './remote-loaders/in-test-raw-loader';
import { getDoubleAgentMockedScriptContent } from './directives/mock';



describe('ValidatorDefinitionsLoader', () => {

  let scriptContent = getDoubleAgentMockedScriptContent();
  let doubleAgentValidator = new DoubleAgentValidator();
  let loader: ValidatorDefinitionsLoader;
  let remoteLoader;
  let jsdom = jsdomNS.jsdom;
  jsdomNS.createVirtualConsole().sendTo(console);

  let window: Window;

  beforeEach(() => {
    remoteLoader = new InTestRawLoader(scriptContent, doubleAgentValidator);
    loader = new ValidatorDefinitionsLoader(remoteLoader);
    window = jsdom('<html><body>Página de Teste</body></html>', { url: 'http://localhost' }).defaultView;
  });

  it('loads script from remote module', (done) => {
    loader.load(window, scriptContent).then(() => {
      doubleAgentValidator = new DoubleAgentValidator();
      doubleAgentValidator['scriptContext'] = window;
      let result = doubleAgentValidator.validate('contribuinte-v1', {
        id: 1,
        nome: 'John',
        ni: '00.000.000/0001-91',
        nacionalidade: 'brasileiro'
      });
      expect(result.hasErrors).toBeFalsy();
      done();
    });
  });
});
