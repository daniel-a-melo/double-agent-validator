import { FormControl } from '@angular/forms';
import { JsonSchemaProperty } from '../models/schema/json-schema-property';

export interface DoubleAgentFormControl extends FormControl {
  propertyName: string;
  jsonSchemaProperty: JsonSchemaProperty;
}
