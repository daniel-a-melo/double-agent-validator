import { DoubleAgentFormGroupBuilder } from './form-group-builder.service';
import { DoubleAgentValidator } from '../validator.service';
import * as jsdomNS from 'jsdom';
import { getDoubleAgentMockedScriptContent } from '../directives/mock';
import { DoubleAgentFormControlValidatorBuilder } from './form-control-validator-builder.service';
import { FormBuilder } from '@angular/forms';

describe(DoubleAgentFormGroupBuilder.name, () => {
  let subject: DoubleAgentValidator;
  let jsdom = jsdomNS.jsdom;
  jsdomNS.createVirtualConsole().sendTo(console);
  let scriptContent = getDoubleAgentMockedScriptContent();
  let window: Window;
  let formGroupBuilder: DoubleAgentFormGroupBuilder;

  beforeAll((done) => {
    window = jsdom('<html><body>Página de Teste<script>' + scriptContent
      + '</script></body></html>', { url: 'http://localhost' }).defaultView;
    window.onload = (ev) => {
      subject = new DoubleAgentValidator();
      subject['scriptContext'] = window;
      done();
    };
  });


  beforeEach(() => {
    formGroupBuilder = new DoubleAgentFormGroupBuilder(subject, new DoubleAgentFormControlValidatorBuilder(subject), new FormBuilder());
  });

  it('builds a formGroup adding validators', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1');
    expect(formGroup.get('ni').validator).not.toBeNull();
  });

  it('builds a formGroup without validators', () => {
    let formGroup = formGroupBuilder.build('contribuinte-v1', { createValidators: false });
    expect(formGroup.get('ni').validator).toBeNull();
  });

});
