export interface JsonSchemaProperty {
  type: string;
  format?: string;
  pattern?: RegExp;
  ui?: any;
  maxLength?: number;
  minLength?: number;
}
