
/** It forces process terminate when debugging tests in Vs Code */
process.on('exit', (code) => {
    // this force terminates the debugger by attaching to the debugger socket via it's FD and destroying it!
    try {
        new require('net').Socket({
            fd: parseInt(
                require('child_process').spawnSync('lsof', ['-np', process.pid], {encoding:'utf8'})
                    .stdout.match(/^.+?\sTCP\s+127.0.0.1:\d+->127.0.0.1:\d+\s+\(ESTABLISHED\)/m)[0].split(/\s+/)[3]
                , 10)
        }).destroy();
    } catch(e) {}
});
