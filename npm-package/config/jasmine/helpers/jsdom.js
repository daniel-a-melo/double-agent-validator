var jsdom = require('jsdom')

var document = jsdom.jsdom('<!doctype html><html><head><title>Jasmine Testing Page</title></head><body></body></html>');

var window = document.defaultView;

global.document = document;
global.HTMLElement = window.HTMLElement;
global.HTMLInputElement = window.HTMLInputElement;
global.XMLHttpRequest = window.XMLHttpRequest;
global.HTMLInputElement = window.HTMLInputElement
global.Node = window.Node;
